require('dotenv').config();
const fs = require('fs').promises;
const pug = require('pug');
const {
	computeLeagueStats,
	computeManagerChartOptions,
	obtainStats,
	obtainLeagueInfo,
} = require('./lib');

const SEASON = '2020';

obtainStats(SEASON, 1, 13)
.then(async teams => {
	// obtain league data
	const leagueInfo = await obtainLeagueInfo(SEASON);

	// compute league stats
	const leagueStats = computeLeagueStats(teams);

	// compute manager chart data
	const managerChartOptions = computeManagerChartOptions(teams);

	// sort teams by id
	teams.sort((a,b) => a.id < b.id ? -1 : 1);
	
	// write to file
	return fs.writeFile('build/index.html', pug.renderFile('src/index.pug', {
		league: { ...leagueInfo, ...leagueStats },
		teams,
		managerChartOptions,
	}));
})
.catch(console.error);