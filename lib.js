const { Client } = require('espn-fantasy-football-api/node');
const fs = require('fs').promises;

const { DATA_DIRECTORY_TEMPLATE, STATS_FILE, ESPN_DATA_FILE, ESPN_S2, SWID, LEAGUE_ID } = process.env;

const espnClient = new Client({ leagueId: LEAGUE_ID });
espnClient.setCookies({ espnS2: ESPN_S2, SWID: SWID });

const obtainStats = async (season, startWeek, endWeek) => {
	// confirm types of inputs
	if (!season || typeof season !== 'string') {
		throw new Error('season missing or invalid');
	}
	if (!startWeek || typeof startWeek !== 'number') {
		throw new Error('startWeek missing or invalid');
	}
	if (!endWeek || typeof endWeek !== 'number') {
		throw new Error('endWeek missing or invalid');
	}
	if (startWeek < 0) {
		throw new Error('start week must be greater than or equal to 0');
	}
	if (endWeek > 18) {
		throw new Error('end week must be less than or equal to 18');
	}
	if (startWeek > endWeek) {
		throw new Error('startWeek must be less than endWeek');
	}

	const data_directory = DATA_DIRECTORY_TEMPLATE.replace('SEASON', season);

	let stats_file = `${data_directory}/${STATS_FILE}`;

	let teams = [];
	try {
		const data = await fs.readFile(stats_file);
		teams = JSON.parse(data);
	} catch (err) {
		if (err.code !== 'ENOENT') {
			console.error('read stats file: ', err);
			throw err;
		}
	}

	if (teams.length === 0 || !teams[0].hasOwnProperty('scores')) {
		// teams are missing box scores
		let boxScoresPerWeek = {};

		let espn_data_file = `${data_directory}/${ESPN_DATA_FILE}`;

		// does raw espn data exist?
		try {
			const data = await fs.readFile(espn_data_file);
			boxScoresPerWeek = JSON.parse(data);
		} catch (err) {
			if (err.code !== 'ENOENT') {
				throw new Error('read raw data file: ', err.message);
			}

			// obtain stats via API for each week
			for (let week = startWeek; week <= endWeek; week++) {
				console.log(`===== Week ${week} =====`);
				boxScoresPerWeek[week] = await obtainScoresForWeek(espnClient, season, week);
			}

			// write raw data to file
			try {
				await fs.mkdir(espn_data_file.split('/').slice(0,-1).join('/'), { recursive: true });
				await fs.writeFile(espn_data_file, JSON.stringify(boxScoresPerWeek,null,2));
			} catch(err) {
				throw new Error('unable to write raw data to file: ', err.message);
			}
		}

		// obtain teams data
		teams = await obtainTeams(season);

		// add box scores data to teams
		addScoresToTeam(teams, boxScoresPerWeek);

		// add stats for each team
		addTeamStats(teams);

		// sort teams by id
		teams.sort((a,b) => a.id < b.id ? -1 : 1);

		// write stats to file
		try {
			await fs.mkdir(stats_file.split('/').slice(0,-1).join('/'), { recursive: true });
			await fs.writeFile(stats_file, JSON.stringify(teams, null, 2));
		} catch(err) {
			throw new Error('unable to write stats to file: ', err.message);
		}
	}

	return teams;
};

const determineRosterStructure = () => {
	return {
		'QB': 1,
		'RB': 2,
		'WR': 2,
		'TE': 1,
		'RB/WR/TE': 1,
		'D/ST': 1,
		'K': 1,
	};
	// const league = await espnClient.getLeagueInfo({ seasonId });
	// return league.rosterSettings.lineupPositionCount;
};

const determineHighestScoringPlayer = players => {
	let maxScore = Number.MIN_SAFE_INTEGER;
	let highestScoringPlayer = null;
	players.forEach(player => {
		if (player.totalPoints > maxScore) {
			maxScore = player.totalPoints;
			highestScoringPlayer = player;
		}
	});
	return highestScoringPlayer;
};

const removePlayerFromList = (players, player) => {
	players.splice(players.findIndex(p => p.player.id === player.player.id), 1);
};

const computeScoreFromRoster = roster => {
	let score = 0;
	Object.keys(roster).forEach(position => {
		score += roster[position].map(p => p.totalPoints).reduce((a, b) => a + b);
	});
	return score;
};

const obtainTeams = async (seasonId) => {
	return await espnClient.getTeamsAtWeek({ seasonId, scoringPeriodId: '1' });
};

const computeManagerChartOptions = (teams) => {
	// sort teams by percentOfMaxScore
	teams.sort((a,b) => {
		const diff = a.percentOfMaxScore - b.percentOfMaxScore;
		return diff === 0 ? 0 : diff < 0 ? -1 : 1;
	});

	const percents = teams.map(t => (t.percentOfMaxScore * 100).toFixed(2));
	const average = (teams.map(t => t.percentOfMaxScore).reduce((a,b) => a+b) / percents.length * 100).toFixed(2);

	const namedTeams = teams.map(team => {
		const { name, wins, losses, playoffSeed } = team;
		return `${name} (${wins}-${losses}) - ${playoffSeed}`;
	});

	return {
		annotations: {
			xaxis: [
				{
					x: average,
					borderColor: '#FF0000',
					label: {
						text: `League Average - ${average}%`,
						orientation: 'horizontal',
					},
				},
			],
		},
		chart: {
			type: 'bar',
			animations: {
				speed: 400,
			},
			toolbar: {
				show: false,
			},
			height: 350,
		},
		dataLabels: {
			enabled: false,
		},
		plotOptions: {
			bar: {
				horizontal: true,
			}
		},
		series: [{
			name: 'Percent of Max Score',
			data: percents,
		}],
		title: {
			text: 'Percent of Max Score',
			align: 'center',
			margin: 0,
			floating: true,
		},
		xaxis: {
			type: 'category',
			categories: namedTeams,
		},
		yaxis: {
			min: 70,
			max: 95,
			tickAmount: 5,
			labels: {
				maxWidth: 200,
			}
		},
	};
};

const computeMaximumScore = (roster, structure) => {
	const structurePositions = Object.keys(structure);
	const structureLimits = Object.values(structure);

	// find highest score(s) for each position
	const players = roster.slice();
	const idealRoster = {};

	structurePositions.forEach((position,index) => {
		// setup ideal roster
		idealRoster[position] = [];

		while (idealRoster[position].length < structureLimits[index]) {
			// find structureLimits[index] players to fill position(s)
			const eligiblePlayers = players.filter(p => p.player.eligiblePositions.includes(position));
			const player = determineHighestScoringPlayer(eligiblePlayers);

			// add to ideal roster
			idealRoster[position].push(player);

			// remove used players
			removePlayerFromList(players, player);
		}
	});

	// sum up points
	return computeScoreFromRoster(idealRoster);
};

const obtainScoresForWeek = async (espnClient, seasonId, week) => {
	return await espnClient.getBoxscoreForWeek({
		seasonId,
		matchupPeriodId: week,
		scoringPeriodId: week,
	});
};

const addScoreToTeam = (teams, week, id, score, oppositeScore, roster, rosterStructure) => {
	const matchingTeamIndex = teams.findIndex(t => t.id === id);
	if (!teams[matchingTeamIndex].scores) {
		teams[matchingTeamIndex].scores = [];
	}
	teams[matchingTeamIndex].scores.push({
		week,
		actualScore: score,
		maxScore: computeMaximumScore(roster, rosterStructure),
		scoreAgainst: oppositeScore,
	});
};

const addScoresToTeam = (teams, weeklyStats) => {
	const rosterStructure = determineRosterStructure();
	// for each week
	Object.keys(weeklyStats).forEach(week => {
		// for each matchup 
		weeklyStats[week].forEach(({ homeTeamId, homeScore, homeRoster, awayTeamId, awayScore, awayRoster }) => {
			// capture home team data
			addScoreToTeam(teams, week, homeTeamId, homeScore, awayScore, homeRoster, rosterStructure);

			// capture away team data
			addScoreToTeam(teams, week, awayTeamId, awayScore, homeScore, awayRoster, rosterStructure);
		});
	});
};

/* returns
{
	totalActualScore,
	totalMaxScore,
	averageWeeklyActualScore,
	averageWeeklyMaxScore,
	averagePercentOfMax,
}
*/
const computeLeagueStats = teams => {
	if (teams.length === 0) {
		return {};
	}
	const weeks = teams[0].scores.length;

	let totalActualScore = 0;
	let totalMaxScore = 0;
	teams.forEach(team => {
		totalActualScore += team.scores.map(x => x.actualScore).reduce((a,b) => a+b);
		totalMaxScore += team.scores.map(x => x.maxScore).reduce((a,b) => a+b);
	});

	const averageWeeklyActualScore = totalActualScore / teams.length / weeks;
	const averageWeeklyMaxScore = totalMaxScore / teams.length / weeks;
	const averagePercentOfMax = totalActualScore / totalMaxScore;

	return {
		totalActualScore,
		totalMaxScore,
		averageWeeklyActualScore,
		averageWeeklyMaxScore,
		averagePercentOfMax,
	};
};

const addTeamStats = teams => {
	teams.forEach(team => {
		const totalActualScore = team.scores.map(s => s.actualScore).reduce((a,b) => a+b);
		const totalMaxPoints = team.scores.map(s => s.maxScore).reduce((a,b) => a+b);
		team.totalMaxPoints = totalMaxPoints;
		team.totalActualScore = totalActualScore;
		team.totalDifferencePoints = totalMaxPoints - totalActualScore;
		team.averageMaxScore = totalMaxPoints / team.scores.length;
		team.averageActualScore = totalActualScore / team.scores.length;
		team.percentOfMaxScore = totalActualScore / totalMaxPoints;
	});
};

const obtainLeagueInfo = async season => {
	return await espnClient.getLeagueInfo({ seasonId: season });
};

module.exports = {
	addScoresToTeam,
	addScoreToTeam,
	addTeamStats,
	computeLeagueStats,
	computeManagerChartOptions,
	computeMaximumScore,
	determineRosterStructure,
	obtainLeagueInfo,
	obtainScoresForWeek,
	obtainStats,
	obtainTeams,
};