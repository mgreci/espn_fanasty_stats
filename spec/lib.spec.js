require('dotenv').config();
const { Client } = require('espn-fantasy-football-api/node');
const fs = require('fs').promises;
const {
	addScoresToTeam,
	computeLeagueStats,
	computeManagerStats,
	computeWeeklyStats,
	obtainScoresForWeek,
	obtainStats,
	obtainTeams,
} = require('../lib');

describe('computeLeagueStats', () => {
	it('handles empty teams', () => {
		const stats = {};
		expect(computeLeagueStats(stats)).toEqual({});
	});
	it('handles single team, single score', () => {
		const stats = {
			'1': [
				{
					"actualScore": 1,
					"maxScore": 1,
				},
			],
		};
		const expected = {
			totalActualScore: 1,
			totalMaxScore: 1,
			averageWeeklyActualScore: 1,
			averageWeeklyMaxScore: 1,
			averagePercentOfMax: 1,
		};
		expect(computeLeagueStats(stats)).toEqual(expected);
	});
	it('handles single team, multiple scores', () => {
		const stats = {
			'1': [
				{
					"actualScore": 1,
					"maxScore": 1,
				},
				{
					"actualScore": 0,
					"maxScore": 1,
				},
			],
		};
		const expected = {
			totalActualScore: 1,
			totalMaxScore: 2,
			averageWeeklyActualScore: 1/2,
			averageWeeklyMaxScore: 1,
			averagePercentOfMax: 1/2,
		};
		expect(computeLeagueStats(stats)).toEqual(expected);
	});
	it('handles multiples teams, multiple scores', () => {
		const stats = {
			'1': [
				{
					"actualScore": 1,
					"maxScore": 1,
				},
				{
					"actualScore": 0,
					"maxScore": 1,
				},
				{
					"actualScore": 1,
					"maxScore": 1,
				},
			],
			'2': [
				{
					"actualScore": 0,
					"maxScore": 1,
				},
				{
					"actualScore": 0,
					"maxScore": 2,
				},
				{
					"actualScore": 1,
					"maxScore": 3,
				},
			],
		};
		const expected = {
			totalActualScore: 3,
			totalMaxScore: 9,
			averageWeeklyActualScore: 3/2/3,
			averageWeeklyMaxScore: 9/2/3,
			averagePercentOfMax: 3/9,
		};
		expect(computeLeagueStats(stats)).toEqual(expected);
	});
});

describe('computeManagerStats', () => {
	it('returns empty', () => {
		expect(computeManagerStats({})).toEqual({ teamIds: [], percents: [], average: '0.00' });
	});

	it('returns for one team', () => {
		const teamStats = {
			'1': {
				percentOfMax: 0.75,
			},
		};
		const expected = {
			teamIds: [1],
			percents: ['75.00'],
			average: '75.00',
		};
		expect(computeManagerStats(teamStats)).toEqual(expected);
	});

	it('returns for multi teams, sorted', () => {
		const teamStats = {
			'1': {
				percentOfMax: 0.75,
			},
			'2': {
				percentOfMax: 0.5,
			},
		};
		const expected = {
			teamIds: [2,1],
			percents: ['50.00', '75.00'],
			average: '62.50',
		};
		expect(computeManagerStats(teamStats)).toEqual(expected);
	});
});

describe('addScoresToTeam', () => {
	it('handles empty teams', () => {
		const teams = [];
		const weeklyScores = {};
		addScoresToTeam(teams, weeklyScores);
		expect(teams).toEqual([]);
	});
	it('handles single team, single score', () => {
		const teams = [
			{
				id: 1,
			},
			{
				id: 2,
			},
		];
		const weeklyScores = {
			'1': [
				{
					"homeScore": 106.36,
					"homeTeamId": 2,
					"homeRoster": [
						{
							"player": {
								"seasonId": "2020",
								"id": 3117251,
								"firstName": "Christian",
								"fullName": "Christian McCaffrey",
								"lastName": "McCaffrey",
								"jerseyNumber": null,
								"proTeam": "Carolina Panthers",
								"proTeamAbbreviation": "CAR",
								"defaultPosition": "RB",
								"eligiblePositions": [
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": false,
								"isInjured": true,
								"injuryStatus": "OUT"
							},
							"position": "RB",
							"totalPoints": 28.500000000000004,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 9.700000000000001,
								"rushingTouchdowns": 12,
								"receivingYards": 3.8000000000000003,
								"receivingReceptions": 3
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 7.884316424,
								"rushingTouchdowns": 3.8354098800000003,
								"rushing2PtConversions": 0.0436,
								"receivingYards": 5.424683453,
								"receivingTouchdowns": 1.2103197780000001,
								"receiving2PtConversions": 0.02191533,
								"receivingReceptions": 6.353417973,
								"lostFumbles": -0.155398528
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 97,
								"rushingTouchdowns": 2,
								"receivingYards": 38,
								"receivingReceptions": 3
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 78.84316424,
								"rushingTouchdowns": 0.63923498,
								"rushing2PtConversions": 0.0218,
								"receivingYards": 54.24683453,
								"receivingTouchdowns": 0.201719963,
								"receiving2PtConversions": 0.010957665,
								"receivingReceptions": 6.353417973,
								"lostFumbles": 0.077699264
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 15847,
								"firstName": "Travis",
								"fullName": "Travis Kelce",
								"lastName": "Kelce",
								"jerseyNumber": null,
								"proTeam": "Kansas City Chiefs",
								"proTeamAbbreviation": "KC",
								"defaultPosition": "WR",
								"eligiblePositions": [
									"WR/TE",
									"TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": false,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "TE",
							"totalPoints": 17,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 5,
								"receivingTouchdowns": 6,
								"receivingReceptions": 6
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.039719533,
								"rushingTouchdowns": 0.10361646,
								"rushing2PtConversions": 0.000736404,
								"receivingYards": 7.219275026,
								"receivingTouchdowns": 3.9767781419999997,
								"receiving2PtConversions": 0.036207068,
								"receivingReceptions": 5.650198798,
								"lostFumbles": -0.06452143
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 50,
								"receivingTouchdowns": 1,
								"receivingReceptions": 6
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 0.39719533,
								"rushingTouchdowns": 0.01726941,
								"rushing2PtConversions": 0.000368202,
								"receivingYards": 72.19275026,
								"receivingTouchdowns": 0.662796357,
								"receiving2PtConversions": 0.018103534,
								"receivingReceptions": 5.650198798,
								"lostFumbles": 0.032260715
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 16737,
								"firstName": "Mike",
								"fullName": "Mike Evans",
								"lastName": "Evans",
								"jerseyNumber": null,
								"proTeam": "Tampa Bay Buccaneers",
								"proTeamAbbreviation": "TB",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "WR",
							"totalPoints": 7.2,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 0.2,
								"receivingTouchdowns": 6,
								"receivingReceptions": 1
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"receivingYards": 5.980786109,
								"receivingTouchdowns": 2.452400142,
								"receiving2PtConversions": 0.04187781,
								"receivingReceptions": 4.016278585,
								"lostFumbles": -0.03802012
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 2,
								"receivingTouchdowns": 1,
								"receivingReceptions": 1
							},
							"projectedRawStats": {
								"usesPoints": false,
								"receivingYards": 59.80786109,
								"receivingTouchdowns": 0.408733357,
								"receiving2PtConversions": 0.020938905,
								"receivingReceptions": 4.016278585,
								"lostFumbles": 0.01901006
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 4047646,
								"firstName": "A.J.",
								"fullName": "A.J. Brown",
								"lastName": "Brown",
								"jerseyNumber": null,
								"proTeam": "Tennessee Titans",
								"proTeamAbbreviation": "TEN",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "WR",
							"totalPoints": 8.9,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 3.9000000000000004,
								"receivingReceptions": 5
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.16969383970000002,
								"rushingTouchdowns": 0.052714524,
								"rushing2PtConversions": 0.000382942,
								"receivingYards": 6.611369256000001,
								"receivingTouchdowns": 2.548026156,
								"receiving2PtConversions": 0.031221022,
								"receivingReceptions": 4.308470163,
								"lostFumbles": -0.060616386
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 39,
								"receivingReceptions": 5
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 1.696938397,
								"rushingTouchdowns": 0.008785754,
								"rushing2PtConversions": 0.000191471,
								"receivingYards": 66.11369256,
								"receivingTouchdowns": 0.424671026,
								"receiving2PtConversions": 0.015610511,
								"receivingReceptions": 4.308470163,
								"lostFumbles": 0.030308193
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 2977187,
								"firstName": "Cooper",
								"fullName": "Cooper Kupp",
								"lastName": "Kupp",
								"jerseyNumber": null,
								"proTeam": "Los Angeles Rams",
								"proTeamAbbreviation": "LAR",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "RB/WR/TE",
							"totalPoints": 8,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 4,
								"receivingReceptions": 4
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.1372660189,
								"rushingTouchdowns": 0.050862828,
								"rushing2PtConversions": 0.000535532,
								"receivingYards": 6.505673333000001,
								"receivingTouchdowns": 2.161111962,
								"receiving2PtConversions": 0.04100457,
								"receivingReceptions": 5.324085693,
								"lostFumbles": -0.106597716
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 40,
								"receivingReceptions": 4
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 1.372660189,
								"rushingTouchdowns": 0.008477138,
								"rushing2PtConversions": 0.000267766,
								"receivingYards": 65.05673333,
								"receivingTouchdowns": 0.360185327,
								"receiving2PtConversions": 0.020502285,
								"receivingReceptions": 5.324085693,
								"lostFumbles": 0.053298858
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 4035538,
								"firstName": "David",
								"fullName": "David Montgomery",
								"lastName": "Montgomery",
								"jerseyNumber": null,
								"proTeam": "Chicago Bears",
								"proTeamAbbreviation": "CHI",
								"defaultPosition": "RB",
								"eligiblePositions": [
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "RB",
							"totalPoints": 8.4,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 6.4,
								"receivingYards": 1,
								"receivingReceptions": 1
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 5.14711844,
								"rushingTouchdowns": 1.852516974,
								"rushing2PtConversions": 0.023872508,
								"receivingYards": 1.3352483810000002,
								"receivingTouchdowns": 0.29179385999999996,
								"receiving2PtConversions": 0.00427853,
								"receivingReceptions": 1.628235819,
								"lostFumbles": -0.110068944
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 64,
								"receivingYards": 10,
								"receivingReceptions": 1
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 51.4711844,
								"rushingTouchdowns": 0.308752829,
								"rushing2PtConversions": 0.011936254,
								"receivingYards": 13.35248381,
								"receivingTouchdowns": 0.04863231,
								"receiving2PtConversions": 0.002139265,
								"receivingReceptions": 1.628235819,
								"lostFumbles": 0.055034472
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 2576623,
								"firstName": "DeVante",
								"fullName": "DeVante Parker",
								"lastName": "Parker",
								"jerseyNumber": null,
								"proTeam": "Miami Dolphins",
								"proTeamAbbreviation": "MIA",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 8.7,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 4.7,
								"receivingReceptions": 4
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"receivingYards": 6.241568383000001,
								"receivingTouchdowns": 1.8108226379999999,
								"receiving2PtConversions": 0.02925853,
								"receivingReceptions": 4.396797108,
								"lostFumbles": -0.03280536
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 47,
								"receivingReceptions": 4
							},
							"projectedRawStats": {
								"usesPoints": false,
								"receivingYards": 62.41568383,
								"receivingTouchdowns": 0.301803773,
								"receiving2PtConversions": 0.014629265,
								"receivingReceptions": 4.396797108,
								"lostFumbles": 0.01640268
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 3052117,
								"firstName": "Phillip",
								"fullName": "Phillip Lindsay",
								"lastName": "Lindsay",
								"jerseyNumber": null,
								"proTeam": "Denver Broncos",
								"proTeamAbbreviation": "DEN",
								"defaultPosition": "RB",
								"eligiblePositions": [
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 4.5,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 2.4000000000000004,
								"receivingYards": 1.1,
								"receivingReceptions": 1
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 4.287908289,
								"rushingTouchdowns": 1.52558229,
								"rushing2PtConversions": 0.018388918,
								"receivingYards": 1.5635563980000002,
								"receivingTouchdowns": 0.384806208,
								"receiving2PtConversions": 0.006776568,
								"receivingReceptions": 2.263516574,
								"lostFumbles": -0.049109752
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 24,
								"receivingYards": 11,
								"receivingReceptions": 1
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 42.87908289,
								"rushingTouchdowns": 0.254263715,
								"rushing2PtConversions": 0.009194459,
								"receivingYards": 15.63556398,
								"receivingTouchdowns": 0.064134368,
								"receiving2PtConversions": 0.003388284,
								"receivingReceptions": 2.263516574,
								"lostFumbles": 0.024554876
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 16913,
								"firstName": "James",
								"fullName": "James White",
								"lastName": "White",
								"jerseyNumber": null,
								"proTeam": "New England Patriots",
								"proTeamAbbreviation": "NE",
								"defaultPosition": "RB",
								"eligiblePositions": [
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 8.2,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 2.2,
								"receivingYards": 3,
								"receivingReceptions": 3
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 2.0423978120000004,
								"rushingTouchdowns": 0.9854244480000001,
								"rushing2PtConversions": 0.00823594,
								"receivingYards": 3.275822719,
								"receivingTouchdowns": 1.182786756,
								"receiving2PtConversions": 0.017094294,
								"receivingReceptions": 3.472184209,
								"lostFumbles": -0.054243858
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 22,
								"receivingYards": 30,
								"receivingReceptions": 3
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 20.42397812,
								"rushingTouchdowns": 0.164237408,
								"rushing2PtConversions": 0.00411797,
								"receivingYards": 32.75822719,
								"receivingTouchdowns": 0.197131126,
								"receiving2PtConversions": 0.008547147,
								"receivingReceptions": 3.472184209,
								"lostFumbles": 0.027121929
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 3916945,
								"firstName": "Darius",
								"fullName": "Darius Slayton",
								"lastName": "Slayton",
								"jerseyNumber": null,
								"proTeam": "New York Giants",
								"proTeamAbbreviation": "NYG",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 28.200000000000003,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 10.200000000000001,
								"receivingTouchdowns": 12,
								"receivingReceptions": 6
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"receivingYards": 4.5071036200000005,
								"receivingTouchdowns": 1.51659726,
								"receiving2PtConversions": 0.023379464,
								"receivingReceptions": 3.283708289,
								"lostFumbles": -0.03976792,
								"kickoffReturnTouchdown": 0.007473378000000001
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 102,
								"receivingTouchdowns": 2,
								"receivingReceptions": 6
							},
							"projectedRawStats": {
								"usesPoints": false,
								"receivingYards": 45.0710362,
								"receivingTouchdowns": 0.25276621,
								"receiving2PtConversions": 0.011689732,
								"receivingReceptions": 3.283708289,
								"lostFumbles": 0.01988396,
								"kickoffReturnTouchdown": 0.001245563
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 2574808,
								"firstName": "Robby",
								"fullName": "Robby Anderson",
								"lastName": "Anderson",
								"jerseyNumber": null,
								"proTeam": "Carolina Panthers",
								"proTeamAbbreviation": "CAR",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 25.4,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 11.4,
								"receivingTouchdowns": 6,
								"receiving2PtConversions": 2,
								"receivingReceptions": 6
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.0687538417,
								"rushingTouchdowns": 0.019189086,
								"rushing2PtConversions": 0.000218426,
								"receivingYards": 5.291476934,
								"receivingTouchdowns": 1.8093508440000001,
								"receiving2PtConversions": 0.032762022,
								"receivingReceptions": 3.513027138,
								"lostFumbles": -0.05965272
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 114,
								"receivingTouchdowns": 1,
								"receiving2PtConversions": 1,
								"receivingReceptions": 6
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 0.687538417,
								"rushingTouchdowns": 0.003198181,
								"rushing2PtConversions": 0.000109213,
								"receivingYards": 52.91476934,
								"receivingTouchdowns": 0.301558474,
								"receiving2PtConversions": 0.016381011,
								"receivingReceptions": 3.513027138,
								"lostFumbles": 0.02982636
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 3917792,
								"firstName": "Daniel",
								"fullName": "Daniel Jones",
								"lastName": "Jones",
								"jerseyNumber": null,
								"proTeam": "New York Giants",
								"proTeamAbbreviation": "NYG",
								"defaultPosition": "TQB",
								"eligiblePositions": [
									"QB",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "WAIVERS",
								"isDroppable": true,
								"isInjured": true,
								"injuryStatus": "OUT"
							},
							"position": "QB",
							"totalPoints": 17.36,
							"pointBreakdown": {
								"usesPoints": true,
								"passingYards": 11.16,
								"passingTouchdowns": 8,
								"passingInterceptions": -4,
								"rushingYards": 2.2
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"passingYards": 9.63672646,
								"passingTouchdowns": 5.26295594,
								"passing2PtConversions": 0.12171336,
								"passingInterceptions": -1.708539854,
								"rushingYards": 1.6972962020000002,
								"rushingTouchdowns": 0.743044218,
								"rushing2PtConversions": 0.008186806,
								"lostFumbles": -0.803985522
							},
							"rawStats": {
								"usesPoints": false,
								"passingYards": 279,
								"passingTouchdowns": 2,
								"passingInterceptions": 2,
								"rushingYards": 22
							},
							"projectedRawStats": {
								"usesPoints": false,
								"passingYards": 240.9181615,
								"passingTouchdowns": 1.315738985,
								"passing2PtConversions": 0.06085668,
								"passingInterceptions": 0.854269927,
								"rushingYards": 16.97296202,
								"rushingTouchdowns": 0.123840703,
								"rushing2PtConversions": 0.004093403,
								"lostFumbles": 0.401992761
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 3121427,
								"firstName": "Curtis",
								"fullName": "Curtis Samuel",
								"lastName": "Samuel",
								"jerseyNumber": null,
								"proTeam": "Carolina Panthers",
								"proTeamAbbreviation": "CAR",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": true,
								"injuryStatus": "OUT"
							},
							"position": "Bench",
							"totalPoints": 9.3,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.5,
								"receivingYards": 3.8000000000000003,
								"receivingReceptions": 5
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.6257100645000001,
								"rushingTouchdowns": 0.261700866,
								"rushing2PtConversions": 0.002978902,
								"receivingYards": 3.9830597560000003,
								"receivingTouchdowns": 1.74767844,
								"receiving2PtConversions": 0.031645316,
								"receivingReceptions": 3.137637873,
								"lostFumbles": -0.023605946
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 5,
								"receivingYards": 38,
								"receivingReceptions": 5
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 6.257100645,
								"rushingTouchdowns": 0.043616811,
								"rushing2PtConversions": 0.001489451,
								"receivingYards": 39.83059756,
								"receivingTouchdowns": 0.29127974,
								"receiving2PtConversions": 0.015822658,
								"receivingReceptions": 3.137637873,
								"lostFumbles": 0.011802973
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 11122,
								"firstName": "Matt",
								"fullName": "Matt Prater",
								"lastName": "Prater",
								"jerseyNumber": null,
								"proTeam": "Detroit Lions",
								"proTeamAbbreviation": "DET",
								"defaultPosition": "WR/TE",
								"eligiblePositions": [
									"K",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "K",
							"totalPoints": 11,
							"pointBreakdown": {
								"usesPoints": true,
								"madeFieldGoalsFrom40To49": 4,
								"madeFieldGoalsFromUnder40": 6,
								"missedFieldGoals": -1,
								"madeExtraPoints": 2
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"madeFieldGoalsFrom40To49": 2.076447968,
								"madeFieldGoalsFromUnder40": 3.221184306,
								"missedFieldGoals": -0.313216841,
								"madeExtraPoints": 2.213691357
							},
							"rawStats": {
								"usesPoints": false,
								"madeFieldGoalsFrom40To49": 1,
								"madeFieldGoalsFromUnder40": 2,
								"missedFieldGoals": 1,
								"madeExtraPoints": 2
							},
							"projectedRawStats": {
								"usesPoints": false,
								"madeFieldGoalsFrom50Plus": 0.212463618,
								"madeFieldGoalsFrom40To49": 0.519111992,
								"madeFieldGoalsFromUnder40": 1.073728102,
								"missedFieldGoals": 0.313216841,
								"madeExtraPoints": 2.213691357,
								"missedExtraPoints": 0.100719621
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": -16006,
								"firstName": "Cowboys",
								"fullName": "Cowboys D/ST",
								"lastName": "D/ST",
								"jerseyNumber": null,
								"proTeam": "Dallas Cowboys",
								"proTeamAbbreviation": "DAL",
								"defaultPosition": "D/ST",
								"eligiblePositions": [
									"D/ST",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false
							},
							"position": "D/ST",
							"totalPoints": 0,
							"pointBreakdown": {
								"usesPoints": true,
								"defensive0PointsAllowed": 0,
								"defensive1To6PointsAllowed": 0,
								"defensive7To13PointsAllowed": 0,
								"defensive14To17PointsAllowed": 0,
								"defensiveInterceptions": 2,
								"defensiveSacks": 1,
								"defensive28To34PointsAllowed": 0,
								"defensive35To45PointsAllowed": 0,
								"defensive100To199YardsAllowed": 0,
								"defensive200To299YardsAllowed": 0,
								"defensive350To399YardsAllowed": 0,
								"defensive400To449YardsAllowed": -3,
								"defensive450To499YardsAllowed": 0,
								"defensive500To549YardsAllowed": 0,
								"defensiveOver550YardsAllowed": 0
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"defensive0PointsAllowed": 0.052755275000000004,
								"defensive1To6PointsAllowed": 0.190019,
								"defensive7To13PointsAllowed": 0.516051606,
								"defensive14To17PointsAllowed": 0.164016402,
								"defensiveBlockedKickForTouchdowns": 0.015518418,
								"defensiveInterceptions": 1.795501778,
								"defensiveFumbles": 1.14970785,
								"defensiveBlockedKicks": 0.118004246,
								"defensiveSafeties": 0.067150912,
								"defensiveSacks": 2.100299505,
								"kickoffReturnTouchdown": 0.07473375,
								"puntReturnTouchdown": 0.093618,
								"fumbleReturnTouchdown": 0.428342946,
								"interceptionReturnTouchdown": 0.372715806,
								"defensive28To34PointsAllowed": -0.159515952,
								"defensive35To45PointsAllowed": -0.22652265300000002,
								"defensive100To199YardsAllowed": 0.047956839,
								"defensive200To299YardsAllowed": 0.319712258,
								"defensive350To399YardsAllowed": -0.2467779,
								"defensive400To449YardsAllowed": -0.608452392,
								"defensive450To499YardsAllowed": -0.599460485,
								"defensive500To549YardsAllowed": -0.281746428,
								"defensiveOver550YardsAllowed": -0.097212507
							},
							"rawStats": {
								"usesPoints": false,
								"defensive0PointsAllowed": 0,
								"defensive1To6PointsAllowed": 0,
								"defensive7To13PointsAllowed": 0,
								"defensive14To17PointsAllowed": 0,
								"defensiveInterceptions": 1,
								"defensiveSacks": 1,
								"defensive28To34PointsAllowed": 0,
								"defensive35To45PointsAllowed": 0,
								"defensive100To199YardsAllowed": 0,
								"defensive200To299YardsAllowed": 0,
								"defensive350To399YardsAllowed": 0,
								"defensive400To449YardsAllowed": 1,
								"defensive450To499YardsAllowed": 0,
								"defensive500To549YardsAllowed": 0,
								"defensiveOver550YardsAllowed": 0
							},
							"projectedRawStats": {
								"usesPoints": false,
								"defensive0PointsAllowed": 0.010551055,
								"defensive1To6PointsAllowed": 0.04750475,
								"defensive7To13PointsAllowed": 0.172017202,
								"defensive14To17PointsAllowed": 0.164016402,
								"defensiveBlockedKickForTouchdowns": 0.002586403,
								"defensiveInterceptions": 0.897750889,
								"defensiveFumbles": 0.574853925,
								"defensiveBlockedKicks": 0.059002123,
								"defensiveSafeties": 0.033575456,
								"defensiveSacks": 2.100299505,
								"kickoffReturnTouchdown": 0.012455625,
								"puntReturnTouchdown": 0.015603,
								"fumbleReturnTouchdown": 0.071390491,
								"interceptionReturnTouchdown": 0.062119301,
								"defensive28To34PointsAllowed": 0.159515952,
								"defensive35To45PointsAllowed": 0.075507551,
								"defensive100To199YardsAllowed": 0.015985613,
								"defensive200To299YardsAllowed": 0.159856129,
								"defensive350To399YardsAllowed": 0.2467779,
								"defensive400To449YardsAllowed": 0.202817464,
								"defensive450To499YardsAllowed": 0.119892097,
								"defensive500To549YardsAllowed": 0.046957738,
								"defensiveOver550YardsAllowed": 0.013887501
							}
						}
					],
					"awayScore": 153.56,
					"awayTeamId": 1,
					"awayRoster": [
						{
							"player": {
								"seasonId": "2020",
								"id": 4242214,
								"firstName": "Clyde",
								"fullName": "Clyde Edwards-Helaire",
								"lastName": "Edwards-Helaire",
								"jerseyNumber": null,
								"proTeam": "Kansas City Chiefs",
								"proTeamAbbreviation": "KC",
								"defaultPosition": "RB",
								"eligiblePositions": [
									null,
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "QUESTIONABLE"
							},
							"position": "RB",
							"totalPoints": 19.8,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 13.8,
								"rushingTouchdowns": 6
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 4.964941629,
								"rushingTouchdowns": 2.9459581740000003,
								"rushing2PtConversions": 0.021,
								"receivingYards": 2.7868415200000003,
								"receivingTouchdowns": 1.44591753,
								"receiving2PtConversions": 0.013164536,
								"receivingReceptions": 3.202929746,
								"lostFumbles": -0.1294507
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 138,
								"rushingTouchdowns": 1
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 49.64941629,
								"rushingTouchdowns": 0.490993029,
								"rushing2PtConversions": 0.0105,
								"receivingYards": 27.8684152,
								"receivingTouchdowns": 0.240986255,
								"receiving2PtConversions": 0.006582268,
								"receivingReceptions": 3.202929746,
								"lostFumbles": 0.06472535
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 16460,
								"firstName": "Adam",
								"fullName": "Adam Thielen",
								"lastName": "Thielen",
								"jerseyNumber": null,
								"proTeam": "Minnesota Vikings",
								"proTeamAbbreviation": "MIN",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "WR",
							"totalPoints": 31,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 11,
								"receivingTouchdowns": 12,
								"receiving2PtConversions": 2,
								"receivingReceptions": 6
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.15585638170000002,
								"rushingTouchdowns": 0.074692284,
								"rushing2PtConversions": 0.000747492,
								"receivingYards": 7.583928673,
								"receivingTouchdowns": 2.665055022,
								"receiving2PtConversions": 0.05215445,
								"receivingReceptions": 5.273414272,
								"lostFumbles": -0.07803665
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 110,
								"receivingTouchdowns": 2,
								"receiving2PtConversions": 1,
								"receivingReceptions": 6
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 1.558563817,
								"rushingTouchdowns": 0.012448714,
								"rushing2PtConversions": 0.000373746,
								"receivingYards": 75.83928673,
								"receivingTouchdowns": 0.444175837,
								"receiving2PtConversions": 0.026077225,
								"receivingReceptions": 5.273414272,
								"lostFumbles": 0.039018325
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 15835,
								"firstName": "Zach",
								"fullName": "Zach Ertz",
								"lastName": "Ertz",
								"jerseyNumber": null,
								"proTeam": "Philadelphia Eagles",
								"proTeamAbbreviation": "PHI",
								"defaultPosition": "WR",
								"eligiblePositions": [
									"WR/TE",
									"TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "WAIVERS",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "TE",
							"totalPoints": 10.8,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 1.8,
								"receivingTouchdowns": 6,
								"receivingReceptions": 3
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"receivingYards": 5.105446039,
								"receivingTouchdowns": 2.73543105,
								"receiving2PtConversions": 0.075615268,
								"receivingReceptions": 4.642066245,
								"lostFumbles": -0.052787424
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 18,
								"receivingTouchdowns": 1,
								"receivingReceptions": 3
							},
							"projectedRawStats": {
								"usesPoints": false,
								"receivingYards": 51.05446039,
								"receivingTouchdowns": 0.455905175,
								"receiving2PtConversions": 0.037807634,
								"receivingReceptions": 4.642066245,
								"lostFumbles": 0.026393712
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 2508176,
								"firstName": "David",
								"fullName": "David Johnson",
								"lastName": "Johnson",
								"jerseyNumber": null,
								"proTeam": "Houston Texans",
								"proTeamAbbreviation": "HOU",
								"defaultPosition": "RB",
								"eligiblePositions": [
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "RB",
							"totalPoints": 19.9,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 7.7,
								"rushingTouchdowns": 6,
								"receivingYards": 3.2,
								"receivingReceptions": 3
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 5.961779252,
								"rushingTouchdowns": 2.5918789259999997,
								"rushing2PtConversions": 0.0248,
								"receivingYards": 2.6043570880000004,
								"receivingTouchdowns": 1.156496088,
								"receiving2PtConversions": 0.012055534,
								"receivingReceptions": 2.673242383,
								"lostFumbles": -0.161562484
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 77,
								"rushingTouchdowns": 1,
								"receivingYards": 32,
								"receivingReceptions": 3
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 59.61779252,
								"rushingTouchdowns": 0.431979821,
								"rushing2PtConversions": 0.0124,
								"receivingYards": 26.04357088,
								"receivingTouchdowns": 0.192749348,
								"receiving2PtConversions": 0.006027767,
								"receivingReceptions": 2.673242383,
								"lostFumbles": 0.080781242
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 4047650,
								"firstName": "DK",
								"fullName": "DK Metcalf",
								"lastName": "Metcalf",
								"jerseyNumber": null,
								"proTeam": "Seattle Seahawks",
								"proTeamAbbreviation": "SEA",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "WR",
							"totalPoints": 19.5,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 9.5,
								"receivingTouchdowns": 6,
								"receivingReceptions": 4
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.1381247281,
								"rushingTouchdowns": 0.044384004000000005,
								"rushing2PtConversions": 0.000428114,
								"receivingYards": 6.024232813,
								"receivingTouchdowns": 2.894186004,
								"receiving2PtConversions": 0.030305968,
								"receivingReceptions": 4.017357026,
								"lostFumbles": -0.08119716
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 95,
								"receivingTouchdowns": 1,
								"receivingReceptions": 4
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 1.381247281,
								"rushingTouchdowns": 0.007397334,
								"rushing2PtConversions": 0.000214057,
								"receivingYards": 60.24232813,
								"receivingTouchdowns": 0.482364334,
								"receiving2PtConversions": 0.015152984,
								"receivingReceptions": 4.017357026,
								"lostFumbles": 0.04059858
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 2576414,
								"firstName": "Raheem",
								"fullName": "Raheem Mostert",
								"lastName": "Mostert",
								"jerseyNumber": null,
								"proTeam": "San Francisco 49ers",
								"proTeamAbbreviation": "SF",
								"defaultPosition": "RB",
								"eligiblePositions": [
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "RB/WR/TE",
							"totalPoints": 25.1,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 5.6000000000000005,
								"receivingYards": 9.5,
								"receivingTouchdowns": 6,
								"receivingReceptions": 4
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 5.2511075830000005,
								"rushingTouchdowns": 3.0615540359999995,
								"rushing2PtConversions": 0.035403142,
								"receivingYards": 1.032945216,
								"receivingTouchdowns": 0.374489904,
								"receiving2PtConversions": 0.007803864,
								"receivingReceptions": 1.133599779,
								"lostFumbles": -0.102217788
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 56,
								"receivingYards": 95,
								"receivingTouchdowns": 1,
								"receivingReceptions": 4
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 52.51107583,
								"rushingTouchdowns": 0.510259006,
								"rushing2PtConversions": 0.017701571,
								"receivingYards": 10.32945216,
								"receivingTouchdowns": 0.062414984,
								"receiving2PtConversions": 0.003901932,
								"receivingReceptions": 1.133599779,
								"lostFumbles": 0.051108894
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 2330,
								"firstName": "Tom",
								"fullName": "Tom Brady",
								"lastName": "Brady",
								"jerseyNumber": null,
								"proTeam": "Tampa Bay Buccaneers",
								"proTeamAbbreviation": "TB",
								"defaultPosition": "TQB",
								"eligiblePositions": [
									"QB",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "WAIVERS",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "QB",
							"totalPoints": 20.46,
							"pointBreakdown": {
								"usesPoints": true,
								"passingYards": 9.56,
								"passingTouchdowns": 8,
								"passingInterceptions": -4,
								"rushingYards": 0.9,
								"rushingTouchdowns": 6
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"passingYards": 12.172830992,
								"passingTouchdowns": 7.348331468,
								"passing2PtConversions": 0.188112006,
								"passingInterceptions": -1.523902298,
								"rushingYards": 0.1326762542,
								"rushingTouchdowns": 0.550402416,
								"rushing2PtConversions": 0.009537442,
								"lostFumbles": -0.447267638
							},
							"rawStats": {
								"usesPoints": false,
								"passingYards": 239,
								"passingTouchdowns": 2,
								"passingInterceptions": 2,
								"rushingYards": 9,
								"rushingTouchdowns": 1
							},
							"projectedRawStats": {
								"usesPoints": false,
								"passingYards": 304.3207748,
								"passingTouchdowns": 1.837082867,
								"passing2PtConversions": 0.094056003,
								"passingInterceptions": 0.761951149,
								"rushingYards": 1.326762542,
								"rushingTouchdowns": 0.091733736,
								"rushing2PtConversions": 0.004768721,
								"lostFumbles": 0.223633819
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 4241463,
								"firstName": "Jerry",
								"fullName": "Jerry Jeudy",
								"lastName": "Jeudy",
								"jerseyNumber": null,
								"proTeam": "Denver Broncos",
								"proTeamAbbreviation": "DEN",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									null,
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 9.600000000000001,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 5.6000000000000005,
								"receivingReceptions": 4
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"receivingYards": 5.0491153980000005,
								"receivingTouchdowns": 1.726958502,
								"receiving2PtConversions": 0.030412324,
								"receivingReceptions": 3.824538881,
								"lostFumbles": -0.044847752
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 56,
								"receivingReceptions": 4
							},
							"projectedRawStats": {
								"usesPoints": false,
								"receivingYards": 50.49115398,
								"receivingTouchdowns": 0.287826417,
								"receiving2PtConversions": 0.015206162,
								"receivingReceptions": 3.824538881,
								"lostFumbles": 0.022423876
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": -16011,
								"firstName": "Colts",
								"fullName": "Colts D/ST",
								"lastName": "D/ST",
								"jerseyNumber": null,
								"proTeam": "Indianapolis Colts",
								"proTeamAbbreviation": "IND",
								"defaultPosition": "D/ST",
								"eligiblePositions": [
									"D/ST",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false
							},
							"position": "D/ST",
							"totalPoints": 6,
							"pointBreakdown": {
								"usesPoints": true,
								"defensive0PointsAllowed": 0,
								"defensive1To6PointsAllowed": 0,
								"defensive7To13PointsAllowed": 0,
								"defensive14To17PointsAllowed": 0,
								"defensiveSacks": 4,
								"defensive28To34PointsAllowed": 0,
								"defensive35To45PointsAllowed": 0,
								"defensive100To199YardsAllowed": 0,
								"defensive200To299YardsAllowed": 2,
								"defensive350To399YardsAllowed": 0,
								"defensive400To449YardsAllowed": 0,
								"defensive450To499YardsAllowed": 0,
								"defensive500To549YardsAllowed": 0,
								"defensiveOver550YardsAllowed": 0
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"defensive0PointsAllowed": 0.08862407500000001,
								"defensive1To6PointsAllowed": 0.320448628,
								"defensive7To13PointsAllowed": 0.790106148,
								"defensive14To17PointsAllowed": 0.216302824,
								"defensiveBlockedKickForTouchdowns": 0.014925342000000001,
								"defensiveInterceptions": 1.198328806,
								"defensiveFumbles": 1.207988186,
								"defensiveBlockedKicks": 0.113708648,
								"defensiveSafeties": 0.041721376,
								"defensiveSacks": 2.607505211,
								"kickoffReturnTouchdown": 0.07473375,
								"puntReturnTouchdown": 0.093618,
								"fumbleReturnTouchdown": 0.408447108,
								"interceptionReturnTouchdown": 0.35350508399999997,
								"defensive28To34PointsAllowed": -0.088123373,
								"defensive35To45PointsAllowed": -0.05107149899999999,
								"defensive100To199YardsAllowed": 0.106611942,
								"defensive200To299YardsAllowed": 0.58060964,
								"defensive350To399YardsAllowed": -0.221232294,
								"defensive400To449YardsAllowed": -0.39491466,
								"defensive450To499YardsAllowed": -0.275289055,
								"defensive500To549YardsAllowed": -0.12613243799999999,
								"defensiveOver550YardsAllowed": -0.038190103
							},
							"rawStats": {
								"usesPoints": false,
								"defensive0PointsAllowed": 0,
								"defensive1To6PointsAllowed": 0,
								"defensive7To13PointsAllowed": 0,
								"defensive14To17PointsAllowed": 0,
								"defensiveSacks": 4,
								"defensive28To34PointsAllowed": 0,
								"defensive35To45PointsAllowed": 0,
								"defensive100To199YardsAllowed": 0,
								"defensive200To299YardsAllowed": 1,
								"defensive350To399YardsAllowed": 0,
								"defensive400To449YardsAllowed": 0,
								"defensive450To499YardsAllowed": 0,
								"defensive500To549YardsAllowed": 0,
								"defensiveOver550YardsAllowed": 0
							},
							"projectedRawStats": {
								"usesPoints": false,
								"defensive0PointsAllowed": 0.017724815,
								"defensive1To6PointsAllowed": 0.080112157,
								"defensive7To13PointsAllowed": 0.263368716,
								"defensive14To17PointsAllowed": 0.216302824,
								"defensiveBlockedKickForTouchdowns": 0.002487557,
								"defensiveInterceptions": 0.599164403,
								"defensiveFumbles": 0.603994093,
								"defensiveBlockedKicks": 0.056854324,
								"defensiveSafeties": 0.020860688,
								"defensiveSacks": 2.607505211,
								"kickoffReturnTouchdown": 0.012455625,
								"puntReturnTouchdown": 0.015603,
								"fumbleReturnTouchdown": 0.068074518,
								"interceptionReturnTouchdown": 0.058917514,
								"defensive28To34PointsAllowed": 0.088123373,
								"defensive35To45PointsAllowed": 0.017023833,
								"defensive100To199YardsAllowed": 0.035537314,
								"defensive200To299YardsAllowed": 0.29030482,
								"defensive350To399YardsAllowed": 0.221232294,
								"defensive400To449YardsAllowed": 0.13163822,
								"defensive450To499YardsAllowed": 0.055057811,
								"defensive500To549YardsAllowed": 0.021022073,
								"defensiveOver550YardsAllowed": 0.005455729
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 4048244,
								"firstName": "Alexander",
								"fullName": "Alexander Mattison",
								"lastName": "Mattison",
								"jerseyNumber": null,
								"proTeam": "Minnesota Vikings",
								"proTeamAbbreviation": "MIN",
								"defaultPosition": "RB",
								"eligiblePositions": [
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "WAIVERS",
								"isDroppable": true,
								"isInjured": true,
								"injuryStatus": "OUT"
							},
							"position": "Bench",
							"totalPoints": 12,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 5,
								"receivingYards": 3,
								"receivingReceptions": 4
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 2.4314831170000004,
								"rushingTouchdowns": 0.9914244240000001,
								"rushing2PtConversions": 0.00992,
								"receivingYards": 0.6314438288,
								"receivingTouchdowns": 0.164720946,
								"receiving2PtConversions": 0.003223548,
								"receivingReceptions": 0.728075843,
								"lostFumbles": -0.052820826
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 50,
								"receivingYards": 30,
								"receivingReceptions": 4
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 24.31483117,
								"rushingTouchdowns": 0.165237404,
								"rushing2PtConversions": 0.00496,
								"receivingYards": 6.314438288,
								"receivingTouchdowns": 0.027453491,
								"receiving2PtConversions": 0.001611774,
								"receivingReceptions": 0.728075843,
								"lostFumbles": 0.026410413
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 13295,
								"firstName": "Emmanuel",
								"fullName": "Emmanuel Sanders",
								"lastName": "Sanders",
								"jerseyNumber": null,
								"proTeam": "New Orleans Saints",
								"proTeamAbbreviation": "NO",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 10.5,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 1.5,
								"receivingTouchdowns": 6,
								"receivingReceptions": 3
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 0.0777674102,
								"rushingTouchdowns": 0.036734946,
								"rushing2PtConversions": 0.000397598,
								"receivingYards": 4.669028091,
								"receivingTouchdowns": 2.1055438680000003,
								"receiving2PtConversions": 0.031885578,
								"receivingReceptions": 3.579661954,
								"lostFumbles": -0.032335292
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 15,
								"receivingTouchdowns": 1,
								"receivingReceptions": 3
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 0.777674102,
								"rushingTouchdowns": 0.006122491,
								"rushing2PtConversions": 0.000198799,
								"receivingYards": 46.69028091,
								"receivingTouchdowns": 0.350923978,
								"receiving2PtConversions": 0.015942789,
								"receivingReceptions": 3.579661954,
								"lostFumbles": 0.016167646
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 15920,
								"firstName": "Latavius",
								"fullName": "Latavius Murray",
								"lastName": "Murray",
								"jerseyNumber": null,
								"proTeam": "New Orleans Saints",
								"proTeamAbbreviation": "NO",
								"defaultPosition": "RB",
								"eligiblePositions": [
									"RB",
									"RB/WR",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 4.800000000000001,
							"pointBreakdown": {
								"usesPoints": true,
								"rushingYards": 4.800000000000001
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"rushingYards": 3.0641330780000002,
								"rushingTouchdowns": 1.73140803,
								"rushing2PtConversions": 0.018739804,
								"receivingYards": 0.7931690616,
								"receivingTouchdowns": 0.22619609400000001,
								"receiving2PtConversions": 0.00342543,
								"receivingReceptions": 1.093106012,
								"lostFumbles": -0.051564076
							},
							"rawStats": {
								"usesPoints": false,
								"rushingYards": 48
							},
							"projectedRawStats": {
								"usesPoints": false,
								"rushingYards": 30.64133078,
								"rushingTouchdowns": 0.288568005,
								"rushing2PtConversions": 0.009369902,
								"receivingYards": 7.931690616,
								"receivingTouchdowns": 0.037699349,
								"receiving2PtConversions": 0.001712715,
								"receivingReceptions": 1.093106012,
								"lostFumbles": 0.025782038
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 3915511,
								"firstName": "Joe",
								"fullName": "Joe Burrow",
								"lastName": "Burrow",
								"jerseyNumber": null,
								"proTeam": "Cincinnati Bengals",
								"proTeamAbbreviation": "CIN",
								"defaultPosition": "TQB",
								"eligiblePositions": [
									null,
									"QB",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": true,
								"injuryStatus": "INJURY_RESERVE"
							},
							"position": "Bench",
							"totalPoints": 16.32,
							"pointBreakdown": {
								"usesPoints": true,
								"passingYards": 7.72,
								"passingInterceptions": -2,
								"rushingYards": 4.6000000000000005,
								"rushingTouchdowns": 6
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"passingYards": 9.254801,
								"passingTouchdowns": 4.808987912,
								"passing2PtConversions": 0.125208062,
								"passingInterceptions": -1.634851574,
								"rushingYards": 1.654129008,
								"rushingTouchdowns": 0.644411886,
								"rushing2PtConversions": 0.008353056,
								"lostFumbles": -0.497862228
							},
							"rawStats": {
								"usesPoints": false,
								"passingYards": 193,
								"passingInterceptions": 1,
								"rushingYards": 46,
								"rushingTouchdowns": 1
							},
							"projectedRawStats": {
								"usesPoints": false,
								"passingYards": 231.370025,
								"passingTouchdowns": 1.202246978,
								"passing2PtConversions": 0.062604031,
								"passingInterceptions": 0.817425787,
								"rushingYards": 16.54129008,
								"rushingTouchdowns": 0.107401981,
								"rushing2PtConversions": 0.004176528,
								"lostFumbles": 0.248931114
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 4035687,
								"firstName": "Michael",
								"fullName": "Michael Pittman Jr.",
								"lastName": "Pittman Jr.",
								"jerseyNumber": null,
								"proTeam": "Indianapolis Colts",
								"proTeamAbbreviation": "IND",
								"defaultPosition": "RB/WR",
								"eligiblePositions": [
									null,
									"RB/WR",
									"WR",
									"WR/TE",
									"RB/WR/TE",
									"OP",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "Bench",
							"totalPoints": 3,
							"pointBreakdown": {
								"usesPoints": true,
								"receivingYards": 1,
								"receivingReceptions": 2
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"receivingYards": 2.431567912,
								"receivingTouchdowns": 1.34193666,
								"receiving2PtConversions": 0.022172272,
								"receivingReceptions": 1.707390234,
								"lostFumbles": -0.020021398
							},
							"rawStats": {
								"usesPoints": false,
								"receivingYards": 10,
								"receivingReceptions": 2
							},
							"projectedRawStats": {
								"usesPoints": false,
								"receivingYards": 24.31567912,
								"receivingTouchdowns": 0.22365611,
								"receiving2PtConversions": 0.011086136,
								"receivingReceptions": 1.707390234,
								"lostFumbles": 0.010010699
							}
						},
						{
							"player": {
								"seasonId": "2020",
								"id": 2971573,
								"firstName": "Ka'imi",
								"fullName": "Ka'imi Fairbairn",
								"lastName": "Fairbairn",
								"jerseyNumber": null,
								"proTeam": "Houston Texans",
								"proTeamAbbreviation": "HOU",
								"defaultPosition": "WR/TE",
								"eligiblePositions": [
									"K",
									"Bench",
									"IR"
								],
								"availabilityStatus": "ONTEAM",
								"isDroppable": true,
								"isInjured": false,
								"injuryStatus": "ACTIVE"
							},
							"position": "K",
							"totalPoints": 1,
							"pointBreakdown": {
								"usesPoints": true,
								"missedFieldGoals": -1,
								"madeExtraPoints": 2
							},
							"projectedPointBreakdown": {
								"usesPoints": true,
								"madeFieldGoalsFrom40To49": 1.697955484,
								"madeFieldGoalsFromUnder40": 2.65153881,
								"missedFieldGoals": -0.311441227,
								"madeExtraPoints": 2.559113909
							},
							"rawStats": {
								"usesPoints": false,
								"missedFieldGoals": 1,
								"madeExtraPoints": 2
							},
							"projectedRawStats": {
								"usesPoints": false,
								"madeFieldGoalsFrom50Plus": 0.172656033,
								"madeFieldGoalsFrom40To49": 0.424488871,
								"madeFieldGoalsFromUnder40": 0.88384627,
								"missedFieldGoals": 0.311441227,
								"madeExtraPoints": 2.559113909,
								"missedExtraPoints": 0.190920896
							}
						}
					]
				},
			],
		};
		const expected = [
			{
				id: 1,
				scores: [
					{
						week: "1",
						actualScore: 153.56,
						maxScore: 153.56,
					},
				],
			},
			{
				id: 2,
				scores: [
					{
						week: "1",
						actualScore: 106.36,
						maxScore: 145.16000000000003,
					},
				],
			},
		];
		addScoresToTeam(teams, weeklyScores);
		expect(teams).toEqual(expected);
	});
});

describe('obtainScoresForWeek', () => {
	it('calls getBoxscoreForWeek', async () => {
		const espnClient = {
			getBoxscoreForWeek: () => {}
		};
		spyOn(espnClient, 'getBoxscoreForWeek');
		const season = '2020';
		const week = 1;
		await obtainScoresForWeek(espnClient,season, week);
		expect(espnClient.getBoxscoreForWeek).toHaveBeenCalledOnceWith({ seasonId: season, matchupPeriodId: week, scoringPeriodId: week });
	});
});

describe('obtainStats', () => {
	describe('params', () => {
		it('errors on missing season', async () => {
			await expectAsync(obtainStats(null, 1, 1)).toBeRejected();
		});
		it('errors on incorrect season type', async () => {
			await expectAsync(obtainStats(2020, 1, 1)).toBeRejected();
		});
		it('errors on missing startWeek', async () => {
			await expectAsync(obtainStats('2020', null, 1)).toBeRejected();
		});
		it('errors on incorrect startWeek type', async () => {
			await expectAsync(obtainStats('2020', '1', 1)).toBeRejected();
		});
		it('errors on missing endWeek', async () => {
			await expectAsync(obtainStats('2020', 1, null)).toBeRejected();
		});
		it('errors on incorrect endWeek type', async () => {
			await expectAsync(obtainStats('2020', 1, '1')).toBeRejected();
		});
		it('errors on negative startWeek', async () => {
			await expectAsync(obtainStats('2020', -1, 1)).toBeRejected();
		});
		it('errors on too large endWeek', async () => {
			await expectAsync(obtainStats('2020', 1, 19)).toBeRejected();
		});
		it('errors on startWeek > endWeek', async () => {
			await expectAsync(obtainStats('2020', 6, 1)).toBeRejected();
		});
	});

	it('results defined', async () => {
		const result = await obtainStats('2020', 1, 1);
		expect(result).toBeDefined();
	});

	it('stats file exists', async () => {
		spyOn(fs, 'readFile').and.resolveTo('[{"id":1,"scores":[]}]');
		await expectAsync(obtainStats('2020', 1, 1)).toBeResolved();
	});

	it('stats file does NOT exist', async () => {
		spyOn(fs, 'readFile').and.throwError({ code: 'ENOENT' });
		await expectAsync(obtainStats('2020', 1, 1)).toBeResolved();
	});

	it('stats file other error', async () => {
		spyOn(fs, 'readFile').and.throwError({ code: 'OTHER', message: 'other error' });
		await expectAsync(obtainStats('2020', 1, 1)).toBeRejected();
	});

	// it('', async () => {
	// 	const espnClient = {
	// 		getBoxscoreForWeek: () => {}
	// 	};
	// 	spyOn(espnClient, 'getBoxscoreForWeek');
	// 	const season = '2020';
	// 	const week = 1;
	// 	await obtainScoresForWeek(espnClient,season, week);
	// 	expect(espnClient.getBoxscoreForWeek).toHaveBeenCalledOnceWith({ seasonId: season, matchupPeriodId: week, scoringPeriodId: week });
	// });
});

describe('obtainTeams', () => {
	it('returns array of teams', async () => {
		const teams = await obtainTeams('2020');
		expect(Array.isArray(teams)).toBeTrue();
		expect(teams.length).not.toEqual(0);
	});
});